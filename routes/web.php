<?php

use App\Slc;

Route::get('/', function () {
    return view('vendor.adminlte.login');
});

Route::group(['middleware' => ['auth']], function () {
    Route::get('/home', 'Web\HomeController@index')->name('home');
    foreach (File::files(app()->path() . '/Routes/auth') as $file) {
        require $file;
    }
});

Route::get('slc.js', function () {
    $json = json_encode(array_merge(Slc::scriptVariables(), []));
    $js = <<<js
window.Slc = {$json};
js;
    return response($js)->header('Content-Type', 'text/javascript');
})->name('slc.js');

Auth::routes();

Route::get('/teste/{id}','Web\AcompanhamentoController@index');

//Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
