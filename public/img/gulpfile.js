// usando os packages baixados
var gulp     = require('gulp');
var sass     = require('gulp-sass');
var concat   = require('gulp-concat');
var uglify   = require('gulp-uglify');
var jshint   = require('gulp-jshint');
var stylish  = require('jshint-stylish');

// tarefa default
gulp.task('default', ['sass', 'js', 'watch']);

// compilando e minificando sass junto com concat
gulp.task('sass', function(){
    return gulp.src('sass/*.scss')
        .pipe(concat('style.css'))
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(gulp.dest('css'))
});

// minificando e concatenado todos os arquivos js
gulp.task('js', function(){
    return gulp.src('scripts/**/*.js')
        .pipe(concat('app.js'))
        .pipe(uglify())
        .pipe(gulp.dest('js'))
});


// observa as mudanças nas pastas
gulp.task('watch', function(){
    gulp.watch('sass/**/*.scss', ['sass']);
    gulp.watch('scripts/**/*.js', ['js']);
});

// verificando por erros no javascript e notificando
gulp.task('lint', function(){
    return gulp.src('assets/src/js/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter(stylish));
})