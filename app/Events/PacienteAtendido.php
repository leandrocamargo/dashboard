<?php

namespace App\Events;
use App\Models\Consulta;
use App\Models\Paciente;
use App\Models\User;
use App\Service\HistorioService;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Console\Scheduling\Event;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Created by PhpStorm.
 * User: Tmontec
 * Date: 03/09/2018
 * Time: 17:40
 */
class PacienteAtendido implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $username;

    public $message;

    public function __construct($username)
    {
        $this->username = $username;
        $this->message  = "{$username} liked your status";
    }

    public function broadcastOn()
    {
        return ['paciente-atendido'];
    }

}