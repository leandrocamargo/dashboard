<?php

namespace App\Events;

use App\Models\User;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Created by PhpStorm.
 * User: Tmontec
 * Date: 03/09/2018
 * Time: 17:40
 */
class ConsultaStatusChanged implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $user;
    public $data;

    public function __construct(User $user, $data)
    {
      $this->user = $user;
      $this->data = $data;
    }

    public function broadcastOn()
    {
      return new PrivateChannel("App.User.{$this->user->id}");
    }
}