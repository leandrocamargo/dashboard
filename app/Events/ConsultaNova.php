<?php

namespace App\Event;
use App\Models\Consulta;
use App\Models\Paciente;
use App\Models\User;
use App\Service\HistorioService;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Created by PhpStorm.
 * User: Tmontec
 * Date: 03/09/2018
 * Time: 17:40
 */
class ConsultaNova implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $consulta;

    public function __construct(Consulta $consulta)
    {
      $this->consulta = $consulta;
      $this->generateHistorico();
    }

    public function broadcastOn()
    {
      return new PrivateChannel("App.User.{$this->consulta->doutor_id}");
    }

    private function generateHistorico () {

      $user      =  User::findOrFail($this->consulta->doutor_id);
      $paciente  =  Paciente::findOrFail($this->consulta->paciente_id);
      $historico =  new HistorioService();

      if($historico->verifyHistorico($paciente,$user)) {
          $historico->create($paciente,$user);
      }
    }
}