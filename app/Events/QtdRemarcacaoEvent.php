<?php

namespace App\Events;

use App\Models\Consulta;
use App\Service\QtdRemarcacoesService;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class QtdRemarcacaoEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private $service;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Consulta $consulta)
    {
        $this->service    = new QtdRemarcacoesService();
        $this->verificarRemarcacoes($consulta);
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }

    private function verificarRemarcacoes(Consulta $consulta)
    {
        $this->service->verificaRemarcacoes($consulta);
    }
}
