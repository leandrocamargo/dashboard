<?php

namespace App\Events;

use App\Models\Consulta;
use App\Models\Paciente;
use App\Models\User;
use App\Service\HistorioService;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ConsultaNew implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public $consulta;

    public function __construct(Consulta $consulta)
    {
        $this->consulta = $consulta;
        $this->generateHistorico();
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel("App.User.{$this->consulta->doutor_id}");
    }

    private function generateHistorico () {

        $user      =  User::findOrFail($this->consulta->doutor_id);
        $paciente  =  Paciente::findOrFail($this->consulta->paciente_id);
        $historico =  new HistorioService();

        if($historico->verifyHistorico($paciente,$user)) {
            $historico->create($paciente,$user);
        }
    }

}
