<?php

namespace App\Events;

use App\Models\Paciente;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Created by PhpStorm.
 * User: Tmontec
 * Date: 03/09/2018
 * Time: 17:40
 */
class PacienteCreated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $paciente;

    public function __construct(Paciente $paciente)
    {
      $this->paciente = $paciente;
    }
}