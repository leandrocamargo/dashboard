<?php

namespace App\Helpers;

class UserMenu
{
    private $menuAdmin  = [
        [
            'text' => 'Pacientes',
            'url'  => 'pacientes',
            'icon' => 'address-book',
        ]
    ];

    private  $menuDoutor  =  [
        [
            'text' => 'Consultas',
            'url'  => 'consultas',
            'icon' => 'user-md',
        ],
        [
            'text' => 'Pacientes',
            'url'  => 'pacientes',
            'icon' => 'address-book',
        ],
        [
            'text' => 'Tipos de Consultas',
            'url'  => 'tipos-consultas',
            'icon' => 'book',
        ],
        [
            'text' => 'Exames',
            'url'  => 'exames',
            'icon' => 'flask',
        ],
        [
            'text' => 'Medicamentos',
            'url'  => 'medicamentos',
            'icon' => 'medkit',
        ],
        [
            'text' => 'Convenios',
            'url'  => 'convenios',
            'icon' => 'ambulance',
        ],
        [
            'text' => 'Novo usuario',
            'url'  => 'novo-usuario',
            'icon' => 'user',
        ],
    ];

    private  $menuUser  =  [
        [
            'text' => 'Exames',
            'url'  => 'exames',
            'icon' => 'flask',
        ],
        [
            'text' => 'Medicamentos',
            'url'  => 'medicamentos',
            'icon' => 'medkit',
        ],
        [
            'text' => 'Convenios',
            'url'  => 'convenios',
            'icon' => 'ambulance',
        ],
        [
            'text' => 'Novo usuario',
            'url'  => 'novo-usuario',
            'icon' => 'user',
        ],
    ];
    /**
     * @return array
     */
    public function getMenuAdmin(): array
    {
        return $this->menuAdmin;
    }
    /**
     * @return array
     */
    public function getMenuDoutor(): array
    {
        return $this->menuDoutor;
    }
    /**
     * @return array
     */
    public function getMenuUser(): array
    {
        return $this->menuUser;
    }

    public static function getMenu (): array
    {
        $role = GetRole::getRole(1);

        return $role;
    }

}