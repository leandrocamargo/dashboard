<?php

namespace App\Helpers;

use Illuminate\Support\Facades\DB;

class GetRole
{
    public static function getRole($id) {
        $roleName =  DB::table('roles')
            ->join('role_user','roles.id','=','role_user.role_id')
            ->select('roles.name')
            ->where('role_user.user_id','=',$id)
            ->get();

        return $roleName[0]->{'name'};
    }
}