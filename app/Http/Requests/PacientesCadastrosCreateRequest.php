<?php
/**
 * Created by PhpStorm.
 * User: X
 * Date: 12/12/2018
 * Time: 10:07
 */

namespace App\Http\Requests;


use App\Forms\PacienteCadastroForm;

class PacientesCadastrosCreateRequest extends BaseRequest
{
    public function form(): PacienteCadastroForm
    {
        return new PacienteCadastroForm($this);
    }

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nome' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'nome.required' => 'Nome é obrigatório'
        ];
    }

    public function save()
    {

    }
}