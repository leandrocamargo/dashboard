<?php
/**
 * Created by PhpStorm.
 * User: Terminal
 * Date: 23/10/2018
 * Time: 13:36
 */

namespace App\Http\Requests;

use App\Forms\AcompanhamentoForm;

class AcompanhamentoCreateRequest extends BaseRequest
{
    public function form(): AcompanhamentoForm
    {
        return new AcompanhamentoForm($this);
    }

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [

        ];
    }

    public function save()
    {

    }
}