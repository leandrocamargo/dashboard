<?php
/**
 * Created by PhpStorm.
 * User: Terminal
 * Date: 27/11/2018
 * Time: 13:51
 */

namespace App\Http\Requests;


use App\Forms\ResultadoForm;

class ResultadoCreateRequest extends BaseRequest
{
    public function form(): ResultadoForm
    {
        return new ResultadoForm($this);
    }

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return[
            'valor' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'valor.required' => 'O valor é obrigatório'
        ];
    }

    public function save()
    {

    }

}