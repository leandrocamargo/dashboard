<?php
/**
 * Created by PhpStorm.
 * User: X
 * Date: 17/12/2018
 * Time: 14:02
 */

namespace App\Http\Requests;

use App\Forms\MedicamentoForm;

class MedicamentoCreateRequest extends BaseRequest
{
    public function form(): MedicamentoForm
    {
        return new MedicamentoForm($this);
    }

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'principio_ativo' => 'required',
            'laboratorio' => 'nullable',
            'registro' => 'nullable',
            'apresentacao' => 'nullable',
            'tarja' => 'nullable'
        ];
    }

    public function messages()
    {
        return [
            'principio_ativo.required' => 'Princípio ativo é obrigatório'
        ];
    }

    public function save()
    {

    }
}