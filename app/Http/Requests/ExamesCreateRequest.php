<?php
/**
 * Created by PhpStorm.
 * User: X
 * Date: 18/12/2018
 * Time: 12:00
 */

namespace App\Http\Requests;


use App\Forms\ExameForm;

class ExamesCreateRequest extends BaseRequest
{

    public function form(): ExameForm
    {
        return new ExameForm($this);
    }

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nome' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'nome.required' => 'Nome do exame é obrigatório'
        ];
    }

    public function save()
    {

    }
}