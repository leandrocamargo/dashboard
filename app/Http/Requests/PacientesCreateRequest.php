<?php

namespace App\Http\Requests;

use App\Forms\PacienteForm;

class PacientesCreateRequest extends BaseRequest
{
    public function form(): PacienteForm
    {
        return new PacienteForm($this);
    }

    public function authoriza()
    {
        return true;
    }

    public function rules()
    {
        return[
            'nome' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'nome.required' => 'Nome é obrigatório'
        ];
    }

    public function save()
    {

    }
}