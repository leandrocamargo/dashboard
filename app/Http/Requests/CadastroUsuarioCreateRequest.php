<?php

namespace App\Http\Requests;

use App\Forms\CadastroUsuarioForm;

class CadastroUsuarioCreateRequest extends BaseRequest
{
    public function form(): CadastroUsuarioForm
    {
        return new CadastroUsuarioForm($this);
    }

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $id = $this->request->get('id');
        return[
            'name'  => 'required',
            'password' => 'required',
            'email' => 'required|email|unique:users,email,' . $id,
            'crm'   => 'sometimes|unique:users,crm,' . $id,
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'O nome é obrigatório',
            'password.required' => 'O password é obrigatório',
            'email.required' => 'O email é obrigatório',
            'email' => 'Digite um email válido',
            'unique' => ':attribute já existe'
        ];
    }

    public function save()
    {

    }
}