<?php
/**
 * Created by PhpStorm.
 * User: Terminal
 * Date: 01/11/2018
 * Time: 10:17
 */

namespace App\Http\Requests;


use App\Forms\ExameCaracteristicaForm;

class ExamesCaracteristicasCreateRequest extends BaseRequest
{

    public function form(): ExameCaracteristicaForm
    {
        return new ExameCaracteristicaForm($this);
    }

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nome' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'nome.required' => 'Nome da característica é obrigatório'
        ];
    }

    public function save()
    {

    }
}